
What is Cloud Computing? 

Simply put, cloud computing is the delivery of computing services—including servers, storage, databases, networking, software, analytics, and intelligence—over the Internet (“the cloud”) to offer faster innovation, flexible resources, and economies of scale. You typically pay only for cloud services you use, helping lower your operating costs, run your infrastructure more efficiently and scale as your business needs change.

Top benefits of cloud computing ?

1. COST :

Cloud computing eliminates the capital expense of buying hardware and software and setting up and running on-site 
datacenters—the racks of servers, the round-the-clock electricity for power and cooling, 
the IT experts for managing the infrastructure. 

2. Global Scale :

Startup --> Minimum Customers 2 , 10, 100 ---> 1 lakh 

The benefits of cloud computing services include the ability to scale elastically. In cloud speak, 
that means delivering the right amount of IT resources—for example, more or less computing power, storage, 
bandwidth—right when it is needed and from the right geographic location.

3. Performance :

4. Speed :

5. Security :

6. Productivity :

7. Reliability :

RAID : Redundant Array of Independent Disks 
